package com.notesonjava.auth;

import java.util.Optional;

import io.javalin.http.Context;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserHandler {

	public Optional<String> retrieveUser(Context ctx) {
		Object obj = ctx.attribute("Quizz-User");
		if(obj == null) {
			log.error("Missing user attribute");
			ctx.status(500);
			ctx.result("Failed to retrieve user in method");
			return Optional.empty();
		}else {
			log.info("Retrieved user object : " + obj);
			String user = (String)ctx.attribute("Quizz-User");
			log.info("Retrieved UserInfo : " + user);
			return Optional.of(user);
		}
	}
	
}
